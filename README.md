# Analyzing Cavium ThunderX Hadoop HDFS Usage

Cavium ThunderX Hadoop has 2.1 PB of raw storage capacity. Accounting for HDFS data replication, this is roughly 725 TB of usable storage. The remaining values in this document are in usable size (aka not raw).

As of July 10, 2020, HDFS is at 76% capacity. HDFS has 551 TB used leaving 174 TB of usable free space.

These values were obtained by running `hdfs dfs -df -h` as user `hdfs` from `cavium-rm01`.

### Run Report

The script can be run as user `hdfs` on `cavium-rm01` with the instructions below. The user `hdfs` may need to `kinit` prior to running the script.

```bash
ssh cavium-rm01
git clone <REPO>
cd <REPO>
sudo -s
sudo -u hdfs bash -c './usage-report.sh'
```

### Analysis

Of the directories at the root of HDFS, only 4 directories have usage greater than 100 GB. They are analyzed and the others ignored.

- `/data` - 99% is twitter decahose, remaining is reddit and other common corpuses
- `/user` - user home directories
- `/var` - mostly twitter decahose and qmei twitter data
- `/yarn` - user job logs

Output from July 14, 2020 execution.

```bash
[root@cavium-rm01 thunderx-hdfs-usage (master)]# sudo -u hdfs bash -c './usage-report.sh'

HDFS Filesystem Overview:
Capacity       Used         Free
729.8 TB     555.5 TB     174.3 TB

Largest directories at filesystem root:
253.7 TB   /data
138.0 TB   /user
147.8 TB   /var
 11.7 TB   /yarn

Details of largest directories at filesystem root:

### /data ###
251.1 TB   /data/twitter
  1.5 TB   /data/stage
  0.5 TB   /data/parquet
  0.3 TB   /data/covid
  0.1 TB   /data/si699capstone1
  0.0 TB   /data/test
  0.0 TB   /data/Gutenberg.txt

### /user ###
 36.6 TB   /user/patpark
 28.8 TB   /user/dmal
 14.8 TB   /user/zhaxin
 11.8 TB   /user/luxuan
  7.6 TB   /user/aiwei
  6.9 TB   /user/davaught
  6.6 TB   /user/jwlock

### /var ###
 84.4 TB   /var/twitter
 47.9 TB   /var/si-qmei
  4.5 TB   /var/stage
  4.3 TB   /var/ngrams
  2.9 TB   /var/reddit
  1.6 TB   /var/jthiels
  1.0 TB   /var/smeyer

### /yarn ###
 10.4 TB   /yarn/apps/luxuan
  0.3 TB   /yarn/apps/smeyer
  0.2 TB   /yarn/apps/patpark
  0.1 TB   /yarn/apps/dmal
  0.1 TB   /yarn/apps/ingomez
  0.1 TB   /yarn/apps/caoa
  0.1 TB   /yarn/apps/tonyzhou
```

### References
- https://www.informit.com/articles/article.aspx?p=2755708&seqNum=4
- https://blog.sleeplessbeastie.eu/2014/11/25/how-to-create-simple-bar-charts-in-terminal-using-awk/
