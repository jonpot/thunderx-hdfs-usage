#!/bin/bash
echo -e "\nHDFS Filesystem Overview:\nCapacity       Used         Free"
hdfs dfs -df 2> /dev/null | grep -i hdfs | awk '{printf "%5.1f TB  %8.1f TB  %8.1f TB\n", $2/1024/1024/1024/1024/3, $3/1024/1024/1024/1024/3, $4/1024/1024/1024/1024/3}'

echo -e "\nLargest directories at filesystem root:"
for SUBDIR in data user var yarn; do
  hdfs dfs -du -s /${SUBDIR} 2> /dev/null | awk '{printf "%5.1f TB   %s\n", $1/1024/1024/1024/1024, $2}'
done

echo -e "\nDetails of largest directories at filesystem root:"

for SUBDIR in data user var; do
  echo -e "\n### /${SUBDIR} ###"
  hdfs dfs -du -s "/${SUBDIR}/*" 2> /dev/null | sort -nr | awk '{printf "%5.1f TB   %s\n", $1/1024/1024/1024/1024, $2}' | head -n 7
done

echo -e "\n### /yarn ###"
hdfs dfs -du -s "/yarn/*/*" 2> /dev/null | sort -nr | awk '{printf "%5.1f TB   %s\n", $1/1024/1024/1024/1024, $2}' | head -n 7
